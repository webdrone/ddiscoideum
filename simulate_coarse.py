import scipy as sp
from scipy import linalg as la
import Ddiscoideum_coarse as Agent
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.colors import LogNorm
# import os
import config
# import io
# from contextlib import redirect_stdout
from multiprocessing import Pool, Process
from itertools import repeat
from datetime import datetime


def L_fun(x, t, cfns, locs):
    L_x = config.camp_conc(x, t, cfns, locs)[0]
    return L_x


def L(x):
    x = sp.atleast_2d(x)
    Lx = la.norm(x, axis=1)
    Lx[0] = 1e-6
    return Lx


def grad_L(x, t, cfns, locs):
    # grad_L_x = config.grad_L_field(x)
    grad_L_x = config.grad_chem_conc(x, t, cfns, locs)[0]
    return grad_L_x
    # return sp.ones((1, 2))


def sim_init(n=1):
    # INITIAL CONDITIONS for E. coli
    global t
    t = int(0 * 100)
    global t_end
    t_end = int(config.t_end * 100)
    global dt
    dt = int(config.dt * 100)
    pos = config.agent_poss
    speed = config.speed
    state = config.state

    outfile = None

    # Calculating error of original position
    # err = sp.linalg.norm(pos)
    # print("Starting average error", err)
    # err = 0
    # ave_traj[0] = sp.array(sp.hstack((t / 100, pos, pos ** 2)))

    # Writing CTMC output to file
    # out_dir = os.path.dirname(os.path.realpath(__file__))
    # out_path = os.path.join(out_dir, config.outname)
    global agentlist
    agentlist = []
    t = int(0 * 100)
    for i in range(n):
        L_agent = 0
        grad_L_agent = sp.array([0] * config.d_env)
        agent = Agent.Ddiscoideum(state=state, parameters=None,
                                  L=L_agent, grad_L=grad_L_agent,
                                  pos=pos[i], speed=speed, outfile=outfile)
        agentlist += [agent]
        # print("L at", agent.pos, "=", L(agent.pos),
        #       "(E. coli position)")
        # print("L at", sp.zeros(2), "=", L(sp.zeros(2)))
        # print("E. coli Y_p: ", agent.Y_p)

    print("Constructed", n, "agents.")
    return agentlist


def a_sim(agent_pos, cfns, locs):
    L_agent = L_fun(agent_pos, t / 100, cfns, locs)
    grad_L_agent = grad_L(agent_pos, t / 100, cfns, locs)
    return L_agent, grad_L_agent


# def a_sim(args):
#     agent, cfns, locs = args
#     L_agent = L_fun(agent.pos, t / 100, cfns, locs)
#     grad_L_agent = grad_L(agent.pos, t / 100, cfns, locs)
#     agent.simulate(dt=config.dt,
#                    L=L_agent, grad_L=grad_L_agent)
#     return agent


def agent_sim(agentlist, ave_traj, trajectory, plot=False):
    global t

    n = len(agentlist)
    cfns = [a.get_camp_rate for a in agentlist]
    locs = sp.array([a.pos for a in agentlist])

    if t % 100 == 0:
        print("time", t / 100, "/", t_end / 100)

    p = Pool(40)
    L_grad_Ls = p.starmap(a_sim, list(zip(locs, repeat(cfns), repeat(locs))))
    p.close()
    p.join()

    for (L_agent, grad_L_agent), agent in zip(L_grad_Ls, agentlist):
        agent.simulate(dt=config.dt,
                       L=L_agent, grad_L=grad_L_agent)

    # for i, agent in enumerate(agentlist):
        # print(L(agent.pos))
        # print(L(agent.pos), agent.Y_p)
        if t % dt == 0:
            # print("Time elapsed:", t, "seconds.")
            ave_traj[t // dt, 0] = t / 100
            ave_traj[t // dt, 1:3] += agent.pos / n
            ave_traj[t // dt, 3:] += (agent.pos ** 2) / n
            trajectory += [sp.hstack((t / 100, sp.hstack([a.pos for a in agentlist])))]

    t += dt

    """
    for i, agent in enumerate(agentlist):
        # Dumping full E. coli trajectory and run/tumble times
        sp.save('traj' + str(i + 1), agent.trajectory)
        sp.save('run' + str(i + 1), agent.t_run)
        sp.save('tumble' + str(i + 1), agent.t_tumble)

        # Plotting
        t_run = []
        t_tumble = []
        if plot:
            # RUN/TUMBLE times histogram
            t_run += agent.t_run
            t_tumble += agent.t_tumble

        # Adding final E. coli position to error count
        # err += sp.linalg.norm(agent.pos)

    if plot:
        # RUN/TUMBLE times histogram
        plt.figure()
        plt.hist(sp.array(t_run))
        plt.title("Run times histogram")
        plt.savefig("OUmeth_" + str(n) + "ddisc_hist_run.png")
        plt.figure()
        plt.hist(sp.array(t_tumble))
        plt.title("Tumble times histogram")
        plt.savefig("OUmeth_" + str(n) + "ddisc_hist_tumble.png")
    # """

    return agentlist, ave_traj, trajectory


def plot_agent(agent):
    # Plotting
    x_min = sp.hstack((agent.trajectory[:, 1],
                       agent.trajectory[:, -1])).min() - 1
    x_max = sp.hstack((agent.trajectory[:, 1],
                       agent.trajectory[:, -1])).max() + 1

    nx = 100  # int((x_max - x_min)/0.1)
    ny = nx
    y_min = x_min
    y_max = x_max

    x = sp.linspace(x_min, x_max, nx)
    y = sp.linspace(y_min, y_max, ny)

    xx, yy = sp.meshgrid(x, y)
    h = L(sp.vstack((xx.flatten(), yy.flatten())).T).reshape(nx, ny)

    fig = plt.figure()
    s = fig.add_subplot(1, 1, 1, xlabel='$x$', ylabel='$y$')
    im = s.imshow(
        h,
        extent=(x[0],
                x[-1],
                y[0],
                y[-1]),
        norm=LogNorm(vmin=h.min(), vmax=h.max()),  # L(sp.zeros(2)),
        origin='lower')
    fig.colorbar(im)

    cs = plt.cm.gray(agent.trajectory[:, 0])[0, 0]
    s.plot(agent.trajectory[0, 1], agent.trajectory[0, -1],
           c='black', marker='*', ms=10)
    s.plot(agent.trajectory[:, 1], agent.trajectory[:, -1],
           color=cs, marker='^', ms=3)

    # print(agent.trajectory)
    print(agent.state)

    print("vel", agent.vel)
    print()
    print("Running statistics (mean, std dev):", len(agent.t_run))
    print(sp.mean(agent.t_run), sp.std(agent.t_run))
    print("Tumbling statistics (mean, std dev):", len(agent.t_tumble))
    print(sp.mean(agent.t_tumble), sp.std(agent.t_tumble))


def animate(poss, ax, show_tracks=True):
    # if show_tracks is on, keep previous t positions in scatter
    lines = ax.lines
    if show_tracks:
        # ax.scatter(poss[:, 0], poss[:, 1], s=2 ** 2)
        for i, l in enumerate(lines):
            xy = l.get_xydata()
            xy = sp.vstack((xy, poss[i]))
            l.set_data(*xy.T)
    # otherwise, replace every t.
    else:
        for i, l in enumerate(lines):
            l.set_data(*poss[i])


def simulation(n_agents, plot=True, show_tracks=False):
    global t
    global t_end
    agentlist = sim_init(n_agents)
    ave_traj = sp.zeros(((t_end // dt) + 1, 5))
    trajectory = []

    def frames():
        nonlocal agentlist
        nonlocal ave_traj
        nonlocal trajectory

        while t < t_end:
            agentlist, ave_traj, trajectory = agent_sim(agentlist, ave_traj, trajectory)
            agent_poss = sp.array([a.pos for a in agentlist])
            yield agent_poss

    if plot:
        # Plotting 2D
        # Create new Figure and an Axes which fills it.
        fig = plt.figure(figsize=(7, 7))
        ax = fig.add_subplot(111)
        ax.set_xlim(config.environment[0])
        ax.set_ylim(config.environment[1])
        for a in agentlist:
            if show_tracks:
                ax.plot([], [])
            else:
                ax.plot([], [], marker='o', markersize=2)
        anim = animation.FuncAnimation(fig, animate, frames=frames,
                                       fargs=(ax, show_tracks),
                                       interval=200)
        plt.show()
        agentlist, ave_traj, trajectory = agent_sim(agentlist, ave_traj, trajectory)
    else:
        while t < t_end:
            agentlist, ave_traj, trajectory = agent_sim(agentlist, ave_traj, trajectory)
    return agentlist, ave_traj, sp.array(trajectory)


if __name__ == '__main__':
    agentlist, ave_traj, trajectory = simulation(int(config.n_agents * 1),
                                                 plot=False)
    print(ave_traj)
    print(sp.array([a.pos for a in agentlist]))
    sp.save('ave_agent_coarse_{}_{}'.format(config.n_agents, datetime.now().strftime('%Y-%m-%d_%H:%M:%S')), ave_traj)
    sp.save('trajectory_coarse_{}_{}'.format(config.n_agents, datetime.now().strftime('%Y-%m-%d_%H:%M:%S')), trajectory)


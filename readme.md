# Software for "Statistical abstraction for multi-scale spatio-temporal systems (TOMACS extension)"

The _Ecoli_ repository at https://bitbucket.org/webdrone/ecoli contains code for abstracting and simulating the _E. coli_ model presented as the first case study in the paper.

The _Ddiscoideum_ repository at https://bitbucket.org/webdrone/ddiscoideum contains code for abstracting and simulating the _D. discoideum_ model presented as the second case study in the paper.

## Dependencies
Both codebases require:

- Python3.5+
- numpy
- scipy
- matplotlib

_Ecoli_ codebase requires:

- pyGPs
- stochpy (https://pypi.python.org/pypi/StochPy)

_Ddiscoideum_ codebase requires:

- GPflow (http://gpflow.readthedocs.io/en/latest/intro.html)

## Computational resources
Note that for _Ecoli_, many independent experiments were run in parallel on a server machine with 64 CPU core.

For _Ddiscoideum_, each experiment is parallelised by spawning multiple (40) processes using default python parallelisation routines. The same 64 CPU core
server executed the code, one experiment at a time.

Generated results are included in the repositories. For the _Ddiscoideum_ case, I would recommend drastically reducing the number of agents (~10 for a normal desktop machine) to check that the code is functional. For the _Ecoli_ case, a moderate amount of trajectories can be generated (~10) on a normal desktop, again to verify functionality. Beyond that, reproducing the attached results would require a couple of days of computing power on a comparable machine to the one used. Note that the simulation makes use of unseeded stochastic functions, so results will vary drastically over small samples.

## Execution pipelines
Instructions below assume you have all necessary dependencies and that `python3 --version` returns a version >=3.5.

### _Ecoli_
Clone the _Ecoli_ repo (https://bitbucket.org/webdrone/ecoli) and navigate to the repo folder.

With the installed dependencies (pyGPs and stochpy), do the following.

- To run simulations of the original (unabstracted) _Ecoli_ model, run
`python3 simulate.py`. This should produce the following new files.
`ave_ecoli.npy`
`traj{i}.npy`
`run{i}.npy`
`tumble{i}.npy`
for each `i` $\in \{1,\dots, 100\}$ _Ecoli_ agent.

- To construct underlying functions for run / tumble probability vs methylation and ligand concentrations (Figure 3), run `pipeline.sh`. This should produce the following new files:
`m_L_prphi_True.npy`
`m_L_prphi_False.npy`
`gpEP_phi_run_FITC_True.pkl`
`gpEP_phi_run_FITC_False.pkl`
and
`coarseEP_traj{i}.npy`
`coarseEP_run{i}.npy`
`coarseEP_tumble{i}.npy`
for each `i` $\in \{1,\dots, 100\}$ _Ecoli_ agent. The plots in Figure 3 will be plotted --- save these from matplotlib's interface, then close them to carry on (the python process will remain paused until the figures are closed and matplotlib is exited).

- To reproduce the plots in Figure 4, one needs to run `python3 phi_regression_EP.py` with the desired `sample_size` parameter value in `l133`.

- To reproduce results in Table 1 and plots in Figure 5, run `python3 plot_histograms.py`.

- To reproduce plots in Figure 6, run `python3 plot_averages.py`. This will produce a pair of plots in Figure 6 (the top one). To plot the other pairs (chemotaxis in other ligand fields, L2 and L3) one should leave the appropriate ligand field uncommented in the function `L_field(x, t)` defined in `l65-83` of `config.py`.

NB: simulations will replace output from previous runs. The output files should be saved in different experiment folders if they are to be retained.

With the chosen ligand field, run
`python3 simulate.py`
`python3 simulate_coarse_EP.py`
and
`python3 plot_averages.py`
again.

### _Ddiscoideum_
Clone the _Ddiscoideum_ repo (https://bitbucket.org/webdrone/ddiscoideum) and navigate to the repo folder.

In l11 of config.py, set the number of agents to simulate. I would recommend something like 5 at first, to make sure everything runs before trying to scale up. On a machine with 48 cores simulations took about 4hrs for 250 agents, so scale accordingly.

In l102 of `simulate.py` and `simulate_coarse.py`, set the number of cores you would like to use in parallel. I ran on a 48-core cluster so I used 40 cores leaving some free for overhead.

With the installed dependencies (GPflow), do the following.

- To run simulations of the original (unabstracted) _Ddiscoideum_ model, run
`python3 simulate.py`. This should produce the following new files:
`ave_agent_{n_agents}_{datetime}.npy`
`trajectory_{n_agents}_{datetime}.npy`.

- To construct underlying functions for the abstraction (Figure 7), run `python3 coarsen_obs_dist.py`. This should produce the following new files:
`x_prphi_reltheta_True.npy`
`x_prphi_reltheta_False.npy`

then run `python3 phi_regression.py` which should plot the graphs in Figure 7, and produce the following new files:
`VGP_phi_True.pkl`
`VGP_phi_False.pkl`.

- To simulate the abstracted _Ddiscoideum_ agents run `python3 simulate_coarse.py`. This will produce the following new files:
`ave_agent_{n_agents}_{datetime}_coarse.npy`
`trajectory_{n_agents}_{datetime}_coarse.npy`.

- To plot Figures 8 and 9, run `python3 plot_population_KDE.py`, using the filename of the simulation to be plotted in l124 (`trajectory_{n_agents}_{datetime}.npy` or `trajectory_coarse_{n_agents}_{datetime}.npy` created previously).

- To plot the graphs in Figure 10, you need several experiments of fine and coarse simulations. You can adjust the bash scripts `simulate_fine_many.sh` and `simulate_coarse_many.sh` to run `python3 simulate.py` and `python3 simulate_coarse.py` 4 more times (to have 5 experiments of each) on your machine. Create two new directories (`experiments/original_250` and `experiments/coarse_250`) and move output files from the original and coarse simulations under their respective directory. Once done, run `python3 results_KS.py` which will produce the graphs in Figure 10.

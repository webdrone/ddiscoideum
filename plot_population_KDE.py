import scipy as sp
from scipy.stats import gaussian_kde
from matplotlib import pyplot as plt
from scipy.integrate import dblquad
from scipy.integrate import quad
import os


def KDE_at_times(ftraj=None, n_plots=None, plot=True, entropy=True):
    if ftraj is None:
        ftraj = 'trajectory_500_1.npy'
    if n_plots is None:
        n_plots = 6  # must be divisible by p_cols
    p_cols = 2

    atraj = sp.load(ftraj)
    entropies = []

    fig, axs = plt.subplots(n_plots // p_cols, p_cols)
                            # figsize=((n_plots // p_cols * 2 + 1), p_cols * 2))
    ax_idx = 0
    # xmin = atraj[:, 1:].min()
    # xmax = atraj[:, 1:].max()
    # ymin = xmin
    # ymax = xmax
    xmin = -0.15
    xmax = -xmin
    ymin = xmin
    ymax = xmax

    def plot_kernel(values, kernel, t=0):
        m1, m2 = values

        X, Y = sp.mgrid[xmin:xmax:100j, ymin:ymax:100j]
        positions = sp.vstack([X.ravel(), Y.ravel()])
        Z = sp.reshape(kernel(positions).T, X.shape)

        ax = axs[ax_idx // p_cols, ax_idx % p_cols]
        kax = ax.imshow(sp.rot90(Z), cmap=plt.cm.gist_earth_r,
                        extent=[xmin, xmax, ymin, ymax], vmin=0, vmax=150)
        ax.plot(m1, m2, 'k.', markersize=2)
        ax.set_xlim([xmin, xmax])
        ax.set_ylim([ymin, ymax])
        ax.set_title('t={}min'.format(t), size=9)
        return kax

    def diffh(y, x, density):
        """
        Function to be integrated to estimate density
        S_x diffh(x) = H (entropy of x distribution)
        diffh(x) = - p(x) * ln(p(x))
        """

        z = sp.array((x, y))
        p_z = density.pdf(z)
        if p_z == 0:
            dh = 0
        else:
            dh = - p_z * density.logpdf(z)
        return dh

    for t in range(n_plots):
        if t == n_plots - 1:
            t_idx = -1
        else:
            t_idx = t * len(atraj) // n_plots
        traj_t = atraj[t_idx, 1:]
        traj_t = traj_t.reshape(len(traj_t) // 2, 2).T

        kernel = gaussian_kde(traj_t)

        if entropy:
            # integration for entropy of the kernel density estimate
            # S_x S_y diffh(y, x)
            entr = dblquad(diffh, a=-sp.inf, b=sp.inf,
                           gfun=lambda x: -sp.inf, hfun=lambda x: sp.inf,
                           args=(kernel,))

            print("Entropy at time ", atraj[t_idx, 0])
            print(entr[0], "+/-{}".format(entr[1]))
            print()

            entropies += [(atraj[t_idx, 0], entr[0])]

        if plot:
            kax = plot_kernel(traj_t, kernel, t=atraj[t_idx, 0])

        ax_idx += 1

    if plot:
        plt.tight_layout()
        fig.colorbar(kax, ax=axs.ravel().tolist())
        ftrajdir, ftrajname = os.path.split(ftraj)
        fsavename = 'aggregation_{}.png'.format(ftrajname)
        fig.savefig(os.path.join(ftrajdir, fsavename))
        fig.suptitle('Kernel density estimation of agent position distribution,\n{} agents in original model.'.format(
            (atraj.shape[1] - 1) // 2))
        # plt.show()

    entropies = sp.array(entropies)
    return entropies


if __name__ == '__main__':
    """
    # many files
    # experiments directory
    dirname = os.path.abspath('experiments/original_500')
    directory = os.fsencode(dirname)

    flist = [f for f in os.listdir(
        directory) if os.fsdecode(f).endswith(".npy")]

    for i, file in enumerate(flist):
        fname = os.fsdecode(file)
        if fname.endswith(".npy"):
            print("Processing {}/{}, file: {}".format(i + 1, len(flist), fname))
            fabsname = str(os.path.join(dirname, fname))
            KDE_at_times(ftraj=fabsname, plot=True, entropy=False)
    # """

    # """
    # single file
    fname = 'experiments/coarse_250/trajectory_coarse_250_2018-09-01_02:26:39.npy'
    KDE_at_times(ftraj=fname, plot=True, entropy=False)
    # """

    plt.show()

# Creating P_matrix for Ddiscoideum transitions
# according to SI from Discrete Modeling of Amoeboid Locomotion
# and Chemotaxis in Dictyostelium discoideum by
# Tracking Pseudopodium Growth Direction
# Zahra Eidi
# (https://static-content.springer.com/esm/art%3A10.1038%2Fs41598-017-12656-1/MediaObjects/41598_2017_12656_MOESM1_ESM.pdf)

import scipy as sp

savepath = 'Q_pseudopodium.npy'

alpha = 0.5
beta = 0.5
pr = 1 / 7

n_states = 6


def idf(n, m):
    n = (n + n_states) % n_states
    m = (m + n_states) % n_states
    return int(n == m)


def P_generated():
    P = sp.zeros((n_states ** 2, n_states ** 2))

    for i in range(P.shape[0]):
        n1, m1 = i // n_states, i % n_states
        for j in range(P.shape[1]):
            n2, m2 = j // n_states, j % n_states
            Pt = 0

            Pt += (1 - pr) * idf(m2, n1) * (
                idf(n2, n1 + 1) * (alpha * idf(n1, m1 + 1) + beta * idf(n1, m1 - 1))
                + idf(n2, n1 - 1) *
                (beta * idf(n1, m1 + 1) + alpha * idf(n1, m1 - 1))
            )

            Pt += ((1 - pr) / 2) * idf(m2, n1) * (
                (idf(n2, n1 + 1) + idf(n2, n1 - 1)) *
                (1 - idf(n1, m1)) * (1 - idf(n1, m1 + 1)) * (1 - idf(n1, m1 - 1))
            )

            Pt += (pr / 3) * idf(m2, n1) * (
                (idf(n1, m1 + 1) + idf(n1, m1 - 1)) * (1 - idf(n2, n1)) *
                (1 - idf(n2, n1 + 1)) * (1 - idf(n2, n1 - 1))
            )

            Pt += (pr / 3) * idf(m2, n1) * (
                (1 - idf(n1, m1 - 1)) * (1 - idf(n1, m1)) * (1 - idf(n1, m1 + 1))
                * (1 - idf(n2, n1)) * (1 - idf(n2, n1 + 1)) *
                (1 - idf(n2, n1 - 1))
            )

            Pt *= (1 - idf(n1, m1))
            Pt *= (1 - idf(n2, m2))

            P[i, j] = Pt
    return P


def P_typed():
    P = sp.diag(sp.zeros(36))

    A = sp.zeros((6, 6))
    A[:, 0] = sp.array([0,
                        beta * (1 - pr),
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        alpha * (1 - pr)
                        ])

    B = sp.zeros((6, 6))
    B[:, 0] = sp.array([0,
                        pr / 3,
                        pr / 3,
                        pr / 3,
                        pr / 3,
                        pr / 3
                        ])

    C = sp.zeros((6, 6))
    C[:, 0] = sp.array([0,
                        alpha * (1 - pr),
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        beta * (1 - pr)
                        ])

    D = sp.zeros((6, 6))
    D[:, 1] = sp.array([beta * (1 - pr),
                        0,
                        alpha * (1 - pr),
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        (1 - pr) / 2
                        ])

    E = sp.zeros((6, 6))
    E[:, 0] = sp.array([alpha * (1 - pr),
                        0,
                        beta * (1 - pr),
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        (1 - pr) / 2
                        ])

    F = sp.zeros((6, 6))
    F[:, 0] = sp.array([pr / 3,
                        0,
                        pr / 3,
                        pr / 3,
                        pr / 3,
                        pr / 3
                        ])

    G_low = sp.zeros((6, 6))
    G_low[:, 2] = sp.array([pr / 3,
                            pr / 3,
                            0,
                            pr / 3,
                            pr / 3,
                            pr / 3
                            ])

    G_upp = sp.zeros((6, 6))
    G_upp[:, 1] = sp.array([pr / 3,
                            pr / 3,
                            0,
                            pr / 3,
                            pr / 3,
                            pr / 3
                            ])

    H = sp.zeros((6, 6))
    H[:, 2] = sp.array([(1 - pr) / 2,
                        beta * (1 - pr),
                        0,
                        alpha * (1 - pr),
                        (1 - pr) / 2,
                        (1 - pr) / 2
                        ])

    I = sp.zeros((6, 6))
    I[:, 1] = sp.array([(1 - pr) / 2,
                        alpha * (1 - pr),
                        0,
                        beta * (1 - pr),
                        (1 - pr) / 2,
                        (1 - pr) / 2
                        ])

    J_low = sp.zeros((6, 6))
    J_low[:, 3] = sp.array([pr / 3,
                            pr / 3,
                            pr / 3,
                            0,
                            pr / 3,
                            pr / 3
                            ])

    J_upp = sp.zeros((6, 6))
    J_upp[:, 2] = sp.array([pr / 3,
                            pr / 3,
                            pr / 3,
                            0,
                            pr / 3,
                            pr / 3
                            ])

    K = sp.zeros((6, 6))
    K[:, 3] = sp.array([(1 - pr) / 2,
                        (1 - pr) / 2,
                        beta * (1 - pr),
                        0,
                        alpha * (1 - pr),
                        (1 - pr) / 2
                        ])

    L = sp.zeros((6, 6))
    L[:, 2] = sp.array([(1 - pr) / 2,
                        (1 - pr) / 2,
                        alpha * (1 - pr),
                        0,
                        beta * (1 - pr),
                        (1 - pr) / 2
                        ])

    M = sp.zeros((6, 6))
    M[:, 4] = sp.array([pr / 3,
                        pr / 3,
                        pr / 3,
                        pr / 3,
                        0,
                        pr / 3
                        ])

    N = sp.zeros((6, 6))
    N[:, 4] = sp.array([(1 - pr) / 2,
                        (1 - pr) / 2,
                        beta * (1 - pr),
                        alpha * (1 - pr),
                        0,
                        (1 - pr) / 2
                        ])

    O = sp.zeros((6, 6))
    O[:, 3] = sp.array([(1 - pr) / 2,
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        alpha * (1 - pr),
                        0,
                        beta * (1 - pr)
                        ])

    P = sp.zeros((6, 6))
    P[:, 5] = sp.array([beta * (1 - pr),
                        alpha * (1 - pr),
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        0
                        ])

    Q = sp.zeros((6, 6))
    Q[:, 5] = sp.array([pr / 3,
                        pr / 3,
                        pr / 3,
                        pr / 3,
                        pr / 3,
                        0
                        ])

    R = sp.zeros((6, 6))
    R[:, 5] = sp.array([alpha * (1 - pr),
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        (1 - pr) / 2,
                        beta * (1 - pr),
                        0
                        ])

    Om = sp.zeros((6, 6))

    P_matrix = sp.vstack((sp.hstack((Om, A, B, B, B, C)),
                          sp.hstack((D, Om, E, F, F, F)),
                          sp.hstack((G_low, H, Om, I, G_upp, G_upp)),
                          sp.hstack((J_low, J_low, K, Om, L, J_upp)),
                          sp.hstack((M, M, M, N, Om, O)),
                          sp.hstack((P, Q, Q, Q, R, Om))
                          ))
    return P_matrix


if __name__ == '__main__':
    P_matrix = P_generated()
    P_typed_mat = P_typed()

    print('P_gen sum', sp.sum(P_matrix))
    print(sp.sum(P_matrix, axis=1))
    print('P_typed_sum', sp.sum(P_typed_mat))

    if not sp.all(P_matrix == P_typed_mat):
        mis_idx = sp.where(sp.logical_not(P_matrix == P_typed_mat))
        print(len(mis_idx[0]))
        # print(P_matrix[mis_idx])
        # print(P_typed_mat[mis_idx])

    sp.save(savepath, P_matrix)

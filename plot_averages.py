import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

ave_traj = sp.load('ave_agent_100.npy')
# ave_traj_coarse = sp.load('ave_ecoli_coarse.npy')
# ave_traj_coarse = sp.load('ave_agent_coarse.npy')
# ave_traj_coarse_2nd_order = sp.load('ave_ecoli_coarse_2nd_order.npy')


def plot_traj(ave_traj, title="Mean distance to max"):
    mean = sp.sqrt(ave_traj[:, 1]**2 + ave_traj[:, 2]**2).reshape((len(ave_traj), 1))
    # Calculating variance for ave_traj: Var[X] = E[X^2] - (E[X])^2
    # 1/N sum_n (x_n^2 + y_n^2)^(1/2)^2 = sum_n x_n^2 / N + y_n^2 / N
    var = (ave_traj[:, 3] + ave_traj[:, 4]).reshape((len(ave_traj), 1)) - mean ** 2
    plot_ave_traj = sp.hstack((
        ave_traj[:, 0].reshape((len(ave_traj), 1)),
        mean,
        var
    ))
    print(plot_ave_traj.shape)
    print(plot_ave_traj[:5])

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(plot_ave_traj[:, 0], plot_ave_traj[:, 1], c='b')
    ax.plot(plot_ave_traj[:, 0],
            plot_ave_traj[:, 1] + plot_ave_traj[:, 2], c='r')
    ax.plot(plot_ave_traj[:, 0],
            plot_ave_traj[:, 1] - plot_ave_traj[:, 2], c='r')
    ax.set_title(title)
    ax.set_ylabel("Mean distance from origin (mm)")
    ax.set_xlabel("Time (s)")
    ax.set_ylim((-0.01, 3.50))


plot_traj(ave_traj, title="Agent micro")
# plot_traj(ave_traj_coarse, title="Ecoli coarse")
# plot_traj(ave_traj_coarse, title="Agent coarse")
# plot_traj(ave_traj_coarse_2nd_order, title="Ecoli coarse 2nd order")

plt.show()

#!/usr/bin/bash
# Run simulate_coarse.py script N times with nice
source ~/.bashrc
cd ~/Documents/Ddiscoideum
for i in {1..5}
do
  (nice time python3.6 simulate_coarse.py) 2> coarse_time_$i.txt
done

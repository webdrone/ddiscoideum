import scipy as sp
from scipy import linalg as la
from scipy.stats import ks_2samp
import matplotlib.pyplot as plt
import os

# Load trajectories
traj = []

for file in [f for f in os.listdir() if 'traj' in f[:len('traj')]]:
    t = sp.load(file)
    traj += [t]
print(len(traj))

run = []
for file in [f for f in os.listdir() if f.startswith('run')]:
    t = sp.load(file)
    run += [t]
print(len(run))

tumble = []
for file in [f for f in os.listdir() if f.startswith('tumble')]:
    t = sp.load(file)
    tumble += [t]
print(len(tumble))

coarseEP_traj = []
for file in [f for f in os.listdir() if 'coarseEP_traj' in f]:
    t = sp.load(file)
    coarseEP_traj += [t]
print(len(coarseEP_traj))

coarseEP_run = []
for file in [f for f in os.listdir() if f.startswith('coarseEP_run')]:
    t = sp.load(file)
    coarseEP_run += [t]
print(len(coarseEP_run))

coarseEP_tumble = []
for file in [f for f in os.listdir() if f.startswith('coarseEP_tumble')]:
    t = sp.load(file)
    coarseEP_tumble += [t]
print(len(coarseEP_tumble))


def plot_traj(traj, title='Distance (mm) from (0, 0)', ts=4):
    dist_hist_list = []
    t_end = traj[0][-1, 0]
    print("t_end:", t_end)
    t_interval = t_end / ts
    t = t_interval
    while t <= t_end:
        dist_hist = []
        for ecoli in traj:
            for idx, time in enumerate(ecoli[:, 0]):
                if time == t:
                    t_idx = idx
                    break
                if time > t:
                    t_idx = idx - 1
                    break
            dist_hist += [la.norm(ecoli[t_idx, 1:])]
        dist_hist_list += [sp.array(dist_hist)]
        t += t_interval

    # Histogram subplots
    nbins = 15
    binrange = (3.0, 4.5)
    ny = int(ts / 2)
    nx = 2
    f, axarr = plt.subplots(nrows=ny, ncols=nx, sharey=True, sharex=True)
    for i in range(ny):
        for j in range(nx):
            l_idx = i * nx + j
            plt.hist
            axarr[i, j].hist(dist_hist_list[l_idx], bins=nbins,
                             range=binrange, normed=True)
            axarr[i, j].set_title('t =' + str(t_interval * (l_idx + 1)))

    # Fine-tune figure; make subplots farther from each other.
    f.subplots_adjust(wspace=0.1, hspace=0.3)
    f.suptitle(title)
    f.text(0.5, 0.02, 'Distance (mm) from (0, 0)', ha='center')
    f.text(0.05, 0.5, 'Probability density', va='center', rotation='vertical')
    return dist_hist_list


def plot_rt_times(ecolis, title="Mean distance to max", ts=4):
    rt_times_hist = sp.array([])
    for ecoli in ecolis:
        rt_times_hist = sp.hstack((rt_times_hist, ecoli))

    # Histogram subplots
    nbins = 15
    binrange = (0.0, 4.0)
    plt.figure()
    plt.hist(rt_times_hist, bins=nbins, range=binrange, normed=True)
    plt.title(title)
    plt.xlabel('Time (s)')
    plt.ylabel('Probability density')
    return rt_times_hist


time_n_obs = 6

# Fine system
dist_hist_list = plot_traj(traj,
                           title='Distance (mm) distribution from (0, 0), ' +
                           str(len(traj)) +
                           ' D. discoideum fine',
                           ts=time_n_obs)
plt.savefig('hist_dist_fine.png')

run_hist = plot_rt_times(run,
                         title='Run duration (s) distribution, ' +
                         str(len(run)) +
                         ' D. discoideum fine')
plt.savefig('hist_runtime_fine.png')

tumble_hist = plot_rt_times(tumble,
                            title='Tumble duration (s) distribution, ' +
                            str(len(tumble)) +
                            ' D. discoideum fine')
plt.savefig('hist_tumbletime_fine.png')

# Coarse system
dist_coarseEP_hist_list = plot_traj(coarseEP_traj,
                                    title='Distance (mm) distribution from (0, 0), ' +
                                    str(len(coarseEP_traj)) +
                                    ' D. discoideum coarse',
                                    ts=time_n_obs)
plt.savefig('hist_dist_coarseEP.png')

run_coarseEP_hist = plot_rt_times(coarseEP_run,
                                  title='Run duration (s) distribution, ' +
                                  str(len(coarseEP_run)) +
                                  ' D. discoideum coarse')
plt.savefig('hist_runtime_coarseEP.png')

tumble_coarseEP_hist = plot_rt_times(coarseEP_tumble,
                                     title='Tumble duration (s) distribution, ' +
                                     str(len(coarseEP_tumble)) +
                                     ' D. discoideum coarse')
plt.savefig('hist_tumbletime_coarseEP.png')


# KS two-sample tests for comparing to micro-system
# RUN durations
run_hist_KS = ks_2samp(run_hist, run_coarseEP_hist)
print()
print(len(run_hist), len(run_coarseEP_hist))
print('Run durations two-sample KS test:')
print(run_hist_KS)

# TUMBLE durations
tumble_hist_KS = ks_2samp(tumble_hist, tumble_coarseEP_hist)
print()
print(len(tumble_hist), len(tumble_coarseEP_hist))
print('Tumble durations two-sample KS test:')
print(tumble_hist_KS)

# Distance from (0, 0)
t = 0
t_end = traj[0][-1, 0]
t_interval = t_end / time_n_obs
for dist, dist_coarseEP in zip(dist_hist_list, dist_coarseEP_hist_list):
    t += t_interval
    dist_hist_KS = ks_2samp(dist, dist_coarseEP)
    print()
    print('Distance (mm) from (0, 0) at t=' + str(t) + ' two-sample KS test:')
    print(dist_hist_KS)


# plt.show()

import scipy as sp
from matplotlib import pyplot as plt
import plot_population_KDE as plot_KDE
import os


def plot_experiment_entropy(expdirname, label, load=False, force=False):
    # experiments directory
    dirname = os.path.abspath(expdirname)
    directory = os.fsencode(dirname)

    # loading each experiment in directory and calculating entropy at n times
    entropies = []
    n_times = 6

    # if load, only pick the entropy distros, else pick the non-entropy ones
    flist = [f for f in os.listdir(
        directory) if os.fsdecode(f).endswith("entropy.npy")]
    if not load:
        fexistlist = list(flist)
        flist = [f for f in os.listdir(directory) if (
            os.fsdecode(f).endswith(".npy") and f not in fexistlist)]

    for i, file in enumerate(flist):
        fname = os.fsdecode(file)
        print("Processing {}/{}, file: {}".format(i + 1, len(flist), fname))
        fabsname = str(os.path.join(dirname, fname))
        if load:
            times_entrs = sp.load(fabsname)
        else:
            if (not force) and (os.fsencode('{}_entropy.npy'.format(fname))
                                in fexistlist):
                print("Entropy file exists (loading).")
                times_entrs = sp.load('{}_entropy.npy'.format(fabsname))
            else:
                times_entrs = plot_KDE.KDE_at_times(
                    fabsname, n_times, plot=False)
                sp.save('{}_entropy.npy'.format(fabsname), times_entrs)
        times = sp.array(times_entrs[:, 0])
        entropies += [sp.array(times_entrs[:, 1])]

    # means and std dev for entropies at each time.
    entr_means = sp.mean(entropies, axis=0)
    entr_std = sp.std(entropies, axis=0)

    # plotting with errorbars
    # plt.plot(t_entr, entropies)
    plt.errorbar(times, entr_means, yerr=entr_std, fmt='--o', label=label)


def plot_entropies(expdirlist, labels, load=False, force=False):
    # plotting on single figure
    plt.figure()

    # 1 line for each directory
    for expdir, lbl in zip(expdirlist, labels):
        plot_experiment_entropy(expdir, lbl, load)

    plt.title("KDE entropy measure (mean and std across experiments).")
    plt.ylabel("Entropy [nat]")
    plt.xlabel("Time [min]")
    plt.legend()


if __name__ == '__main__':
    expdirlist = [
        'experiments/original_250',
        'experiments/coarse_250',
    ]

    labels = ['Original model', 'Abstracted model']

    # expdirlist = ['experiments/coarse_250']
    # labels = ['Abstracted model']

    plot_entropies(expdirlist, labels, load=False)
    plt.show()

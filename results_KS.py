import scipy as sp
import os
import ndtest
import matplotlib.pyplot as plt


def read_samples(dirlst, n_splits):
    if not len(dirlst) == 2:
        print("Two sample test requires 2 directories")
    samples = []
    for expdir in dirlst:
        dirname = os.path.abspath(expdir)
        directory = os.fsencode(dirname)

        flist = [f for f in os.listdir(
            directory) if os.fsdecode(f).endswith("entropy.npy")]
        flist = [f for f in os.listdir(directory) if (
            os.fsdecode(f).endswith(".npy") and f not in list(flist))]

        sample = []
        for i, file in enumerate(flist):
            fname = os.fsdecode(file)
            print("Processing {}/{}, file: {}".format(i + 1, len(flist), fname))
            fabsname = str(os.path.join(dirname, fname))
            ftraj = sp.load(fabsname)
            fsample = []
            times = []
            dn = len(ftraj) // n_splits
            for n in range(n_splits - 1):
                times += [ftraj[n * dn, 0]]
                temptraj = ftraj[n * dn, 1:]
                fsample += [temptraj.reshape(len(ftraj[n * dn]) // 2, 2)]
            else:
                times += [ftraj[-1, 0]]
                temptraj = ftraj[-1, 1:]
                fsample += [temptraj.reshape(len(ftraj[-1]) // 2, 2)]
                sample += [sp.array(fsample)]
        samples += [sp.array(sample)]
    return samples, sp.array(times)


def gather_x_y(samples, n, aggregate=False):
    if aggregate:
        x1 = sp.array([])
        y1 = sp.array([])
        x2 = sp.array([])
        y2 = sp.array([])
        for i in range(len(samples[0])):
            x1 = sp.hstack((x1, samples[0][i, n, :, 0]))
            y1 = sp.hstack((y1, samples[0][i, n, :, 1]))
            x2 = sp.hstack((x2, samples[1][i, n, :, 0]))
            y2 = sp.hstack((y2, samples[1][i, n, :, 1]))
    else:
        x1 = samples[0][0, n, :, 0]
        y1 = samples[0][0, n, :, 1]
        x2 = samples[1][0, n, :, 0]
        y2 = samples[1][0, n, :, 1]
    return x1, y1, x2, y2


def two_sample_2D_KS_test(dirlst, n_splits, labels, aggregate=False):
    samples, times = read_samples(dirlst, n_splits)
    if aggregate:
        print("Aggregating experiments for test.")
        for n in range(n_splits):
            x1, y1, x2, y2 = gather_x_y(samples, n, aggregate=True)
            ks_test = ndtest.ks2d2s(x1, y1, x2, y2, extra=True)
            # print("Split {} two 2D sample KS test:".format(n))
            # print(ks_test)
            # print()

            r1 = sp.sqrt(x1 ** 2 + y1 ** 2)
            r2 = sp.sqrt(x2 ** 2 + y2 ** 2)
            ks_test = sp.stats.ks_2samp(r1, r2)
            print("Split {} radius two sample KS test:".format(n))
            print(ks_test[::-1])
            print()

            fig, ax = plt.subplots(2, sharex=True)
            ax[0].hist(r1, density=True)
            ax[0].set_ylim(0, 15)
            ax[0].set_ylabel('{} pdf'.format(labels[0]))
            ax[1].hist(r2, density=True)
            ax[1].set_ylim(0, 15)
            ax[1].set_ylabel('{} pdf'.format(labels[1]))
            ax[1].set_xlabel('Binned distance from origin [mm]')
            fig.suptitle('Distance from (0, 0) at t={}m'.format(times[n]))
    else:
        for n in range(n_splits):
            x1, y1, x2, y2 = gather_x_y(samples, n, aggregate=False)
            ks_test = ndtest.ks2d2s(x1, y1, x2, y2, extra=True)
            print("Split {} two 2D sample KS test:".format(n))
            print(ks_test)
            print()

            r1 = sp.sqrt(x1 ** 2 + y1 ** 2)
            r2 = sp.sqrt(x2 ** 2 + y2 ** 2)
            ks_test = sp.stats.ks_2samp(r1, r2)
            print("Split {} radius two sample KS test:".format(n))
            print(ks_test[::-1])
            print()

            fig, ax = plt.subplots(2, sharex=True)
            ax[0].hist(r1, density=True)
            ax[0].set_ylim(0, 15)
            ax[0].set_ylabel('{} pdf'.labels[0])
            ax[1].hist(r2, density=True)
            ax[1].set_ylim(0, 15)
            ax[1].set_ylabel('{} pdf'.labels[1])
            ax[1].set_xlabel('Binned distance from origin [mm]')
            fig.suptitle('Distance from (0, 0) at t={}m'.format(times[n]))


def plot_distance_over_time(dirlst, n_splits, labels, aggregate=False):
    plt.figure()

    samples, times = read_samples(dirlst, n_splits)
    r1_mean = []
    r2_mean = []
    r1_std = []
    r2_std = []
    for n in range(n_splits):
        x1, y1, x2, y2 = gather_x_y(samples, n, aggregate)
        r1_mean += [sp.mean(sp.sqrt(x1 ** 2 + y1 ** 2))]
        r2_mean += [sp.mean(sp.sqrt(x2 ** 2 + y2 ** 2))]
        r1_std += [sp.std(sp.sqrt(x1 ** 2 + y1 ** 2))]
        r2_std += [sp.std(sp.sqrt(x2 ** 2 + y2 ** 2))]
    print(times)

    plt.errorbar(times, r1_mean, yerr=r1_std,
                 capsize=2, fmt='--^', label=labels[0])
    plt.errorbar(times, r2_mean, yerr=r2_std,
                 capsize=2, fmt='--o', label=labels[1])

    plt.title(
        "Mean and standard deviation of distance from (0, 0)\n across {} experiments.".format(len(samples[0])))
    plt.ylabel("Distance [mm]")
    plt.xlabel("Time [min]")
    plt.ylim(0, 0.15)
    plt.legend()


if __name__ == '__main__':
    dirnamelst = ['experiments/original_250',
                  'experiments/coarse_250']
    labels = ['Original model', 'Abstracted model']
    two_sample_2D_KS_test(dirnamelst, n_splits=6,
                          labels=labels, aggregate=True)
    plot_distance_over_time(dirnamelst, 10, labels, aggregate=True)
    plt.show()

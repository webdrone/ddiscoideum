import scipy as sp
from scipy import linalg
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from multiprocessing import Pool, Process
from itertools import repeat

# import Ddiscoideum as Agent
import config
import simulate

# HELPER FUNCTIONS
'''
def L(x):
    L = config.L_field(x)
    return L



def ecoli_sim(n=1, m_L_prphi_True=[], m_L_prphi_False=[], m_iter=1):
    # INITIAL CONDITIONS for E. coli
    t = 0 * 100
    t_end = int(config.t_end * 100)
    dt = int(config.dt * 100)
    pos = config.pos
    vel = config.vel
    state = config.state

    L_x = L(pos)

    # Loop iterations = number of E. coli simulations
    for i in range(n):
        print("E. coli", i + 1, "of", n)

        t = int(0 * 100)
        ecoli = Agent.Ddiscoideum(state=state, parameters=None, L=L_x,
                                  pos=pos, vel=vel, outfile=None)

        # print("L at", ecoli.pos, "=", L(ecoli.pos),
        #       "(E. coli position)")
        # print("L at", sp.zeros(2), "=", L(sp.zeros(2)))
        # print("E. coli Y_p: ", ecoli.Y_p)

        # Adding some noise to starting methylation and ligand concentration
        # to provide support for the
        # observations over the domain of m, L, away from the steady state.
        # print("Starting methylation level:", ecoli.m)
        # ecoli.m += sp.randn() * ecoli.m
        # ecoli.m = max(0, ecoli.m)
        # print("Starting methylation level after noise:", ecoli.m)

        print("Starting ligand:", L(ecoli.pos))
        ecoli.pos += ecoli.pos * sp.randn() / la.norm(ecoli.pos)
        ecoli.L = L(ecoli.pos)
        print("Starting ligand after noise:", L(ecoli.pos))

        while t < t_end:
            # print(L(ecoli.pos))
            # print(L(ecoli.pos), ecoli.Y_p)

            # if t % (5 * 100) == 0:
                # print("randomising", t / 100)
                # ecoli.m += sp.randn() * ecoli.m
                # ecoli.m = max(0, ecoli.m)

                # ecoli.pos += ecoli.pos * sp.randn() / la.norm(ecoli.pos)
                # ecoli.L = L(ecoli.pos)

            m_L_prphi = [0, 0, 0, 0]  # T|T, |T, T|F, |F
            m_i_pos = sp.array(ecoli.pos)
            m_i_meth = ecoli.m

            # m_i_state = dict(ecoli.state)
            for m_i in range(m_iter):
                # Observations conditioned on previous state of cell.
                if ecoli.run_flag:
                    m_L_prphi[1] += 1  # |T
                    given_run = True
                else:
                    m_L_prphi[-1] += 1  # |F
                    given_run = False

                ecoli.pos = m_i_pos

                ecoli.simulate(dt=config.dt, L=L(ecoli.pos))

                if ecoli.run_flag:
                    if given_run:
                        m_L_prphi[0] += 1  # T|T
                else:
                    if given_run:
                        m_L_prphi[2] += 1  # T|F

            if m_L_prphi[1] > 0:
                m_L_prphi_True += []
            m_L_prphi[m_L_prphi_idx] += int(ecoli.run_flag) / m_iter
            t += dt

    return m_L_prphi_True, m_L_prphi_False
# '''


def a_sim(agent_pos, cfns, locs):
    L_agent = simulate.L_fun(agent_pos, t / 100, cfns, locs)
    grad_L_agent = simulate.grad_L(agent_pos, t / 100, cfns, locs)
    return L_agent, grad_L_agent


def L_grad_Ls_at_agents(agentlist):
    global t

    cfns = [a.get_camp_rate for a in agentlist]
    locs = sp.array([a.pos for a in agentlist])

    if t % 100 == 0:
        print("time", t / 100, "/", t_end / 100)

    p = Pool(5)
    L_grad_Ls = p.starmap(a_sim, list(zip(locs, repeat(cfns), repeat(locs))))
    p.close()
    p.join()

    return L_grad_Ls


def observe_agents(n=5, prphi_True=[], prphi_False=[], m=1):
    global t
    t = int(0 * 100)
    global t_end
    t_end = int(config.t_end * 100)
    global dt
    dt = int(config.dt * 100)

    agentlist = simulate.sim_init(n=n)
    # L_grad_Ls = L_grad_Ls_at_agents(agentlist)

    while t < t_end:
        # prphi = [0, 0, 0, 0]  # T|T, |T, T|F, |F
        x_prphi_list = []

        # calculate L, grad_L, at agents' positions (parallel)
        L_grad_Ls = L_grad_Ls_at_agents(agentlist)

        # taking record of relative angle and run flag
        for (L_agent, grad_L_agent), agent in zip(L_grad_Ls, agentlist):
            # normalising theta_rel (angle between gradient and cell)
            # to be \in[-\pi, \pi)
            th = sp.arctan2(grad_L_agent[1], grad_L_agent[0]) - agent.get_theta()
            # th = th - 2 * sp.pi * sp.int_((th + sp.pi) // (2 * sp.pi))
            x_prphi_list += [(bool(agent.run_flag), th)]

            prphi = prphi_False
            if agent.run_flag:
                prphi = prphi_True

            # advancing agents according to L_grad_Ls calculated
            agent.simulate(dt=config.dt,
                           L=L_agent, grad_L=grad_L_agent)

            """
            # \phi = run_flag(agent)
            if agent.run_flag:
                prphi += [sp.hstack((th, 1))]
            else:
                prphi += [sp.hstack((th, 0))]
            # """

            # \phi ::= (th_ag - th_grad < pi / 2)
            th_rel = sp.arctan2(grad_L_agent[1], grad_L_agent[0]) - agent.get_theta()
            # normalising \theta \in (-\pi, \pi)
            th_rel = th_rel - 2 * sp.pi * sp.int_((th_rel + sp.pi) // (2 * sp.pi))
            if sp.absolute(th_rel) < sp.pi / 2:
                prphi += [sp.hstack((th, 1))]
            else:
                prphi += [sp.hstack((th, 0))]

        t += dt

    return prphi_True, prphi_False


def estimate_phi():
    ##############
    # SIMULATION #
    ##############
    x_prphi_True, x_prphi_False = observe_agents(n=config.n_agents)

    x_prphi_True = sp.array(x_prphi_True)
    # print(x_prphi_True)

    x_prphi_False = sp.array(x_prphi_False)
    # print(x_prphi_False)

    # Saving (m, L, pr(phi)) data
    sp.save('x_prphi_reltheta_True', x_prphi_True)
    sp.save('x_prphi_reltheta_False', x_prphi_False)


if __name__ == '__main__':
    estimate_phi()

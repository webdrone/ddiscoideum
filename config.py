import scipy as sp
from scipy import stats
import scipy.linalg as la
from scipy.special import erf

outname = None

# ====================
# Simulation conditions
# ====================
n_agents = 500
d_env = 2
environment = sp.array([[-1, 1]] * d_env) * (0.15 / 2)

# Distribute agents
# Initial agent distribution
agent_dist = sp.random.uniform
agent_poss = []
for d in range(d_env):
    agent_poss += [agent_dist(*environment[d], size=n_agents)]
agent_poss = sp.vstack(agent_poss)
agent_poss = agent_poss.T


t_end = 30  # Simulation running time (mins)
dt = 0.3  # Time resolution (mins)
state = (0, 1)  # in {0, 1, 2, 3, 4, 5} x {0, 1, 2, 3, 4, 5}
# Initial state for Ddisc according to Zahra model
t_hist = 2  # duration of time for which to record camp production rate [min]

# Movement
speed = 0.02  # mm/min

# ====================
# Model parameters
# ====================

# -----------
# Ddisc model values (Zahra Eidi doi:10.1038/s41598-017-12656-1)
# -----------

epsilon_gamma = 1.0
epsilon_Lsize = 0.01  # size of amoeba cell (diametre) [mm]


def R_mat(th=0):
    return sp.array([[sp.cos(th), -sp.sin(th)], [sp.sin(th), sp.cos(th)]])


# Update of the Q matrix in 2d
def Q_update(Q, C, grad_C):
    # n_matrix = R_mat(j * sp.pi / 3) @ sp.array([1, 0])
    n_matrix = sp.hstack([
        sp.array([R_mat(j * sp.pi / 3) @ sp.array([1, 0])] * 6).T
        for j in range(6)])

    # epsilon = (epsilon_gamma * epsilon_Lsize / C) * grad_C

    # using fixed ep value -- uncomment above to fix
    epsilon = 0.04 * grad_C / la.norm(grad_C)
    print(epsilon @ n_matrix)
    epsilon = sp.vstack([epsilon] * len(Q))
    Q_new = Q + (epsilon @ n_matrix)
    return Q_new


# Update of the P_i vector in 2d
def P_update(P, C, grad_C):
    # n_matrix = R_mat(j * sp.pi / 3) @ sp.array([1, 0])
    n_matrix = sp.array([R_mat(j * sp.pi / 3) @ sp.array([1, 0])
                         for j in range(6)]).T

    # epsilon = (epsilon_gamma * epsilon_Lsize / C) * grad_C

    # using fixed ep value -- uncomment above to fix
    if la.norm(grad_C) == 0:
        epsilon = 0.04 * grad_C
    else:
        epsilon = 0.04 * grad_C / la.norm(grad_C)

    # print('epsilon:', epsilon)
    # print('n_matrix:', n_matrix)
    # print(epsilon@n_matrix)
    # The above is 0, but one of these directions is forbidden.
    # print(epsilon @ n_matrix)

    P_new = P + (epsilon @ n_matrix)
    # P_new = P_new
    # print(P_new)
    # P_new[P == 0] = 0
    # print('P_new:', P_new)
    return P_new


def theta_dist():
    t = sp.random.gamma(shape=4, scale=18.32) / 180 * sp.pi
    return t


# ----------
# cAMP diffusion in Dictyostelium discoideum: A Green’s function method
# internal MG parameters
# ----------
lambda1 = 10
lambda2 = 0.18
lambda3 = 463.5
k1 = 0.036  # [min^-1]
k_1 = 0.36  # [min^-1]
k2 = 0.666  # [min^-1]
k_2 = 0.00333  # [min^-1]
ki = 1.7  # [min^-1]


# ====================
# Environment parameters
# ====================

# Chemical concentration L field (2D gaussian pdf in this case)
def L_field(x):
    # For neutral random walk, always same L(0,0)
    # x = sp.zeros(sp.shape(x))

    # Vladimirov et al.
    S = 2e4 * sp.eye(d_env)
    L = (2e2 *
         sp.sqrt(la.det(2 * sp.pi * S)) *  # unnormalised
         stats.multivariate_normal.pdf(x, mean=sp.zeros(d_env), cov=S))

    # K_I = 8
    # L = K_I * sp.exp(-sp.linalg.norm(x, axis=-1)/200)

    # L = 1 * (1 - sp.linalg.norm(x, axis=-1) * 0.001)
    # L = sp.array(L)
    # L[L < 0] = 10**-4
    return L  # + sp.random.normal(scale=0.5)

# Grad of chemical concentration L field (2D gaussian pdf in this case)


def grad_L_field(x):
    # For neutral random walk, always same L(0,0)
    # x = sp.zeros(sp.shape(x))

    # Vladimirov et al.
    grad_L = L_field(x)

    grad_L = grad_L * (sp.zeros(d_env) - x) / la.norm((sp.zeros(d_env) - x))

    # K_I = 8
    # L = K_I * sp.exp(-sp.linalg.norm(x, axis=-1)/200)

    # L = 1 * (1 - sp.linalg.norm(x, axis=-1) * 0.001)
    # L = sp.array(L)
    # L[L < 0] = 10**-4
    return grad_L  # + sp.random.normal(scale=0.5)


# Lesion parameters

# Amoebas emit a chemoattractant (cAMP)
# which diffuses with degradation.
# We set up parameters for the chemo-a production.
# The diffusion PDE (with degradation) admits a solution produced by using
# a Green's function. The solution for the concentration of the chemical
# at any point in space, is given in
# (1)
# D Calovi, L Brunnet, RMC de Almeida,
# cAMP diffusion in Dd: A Green's function method, 10.1103/PhysRevE.82.011909
# and references
# (2)
# E. Butkov, Mathematical Physics (Addison-Wesley, Reading, MA, 1968)
# for the solution (needs checking).
##
# γ(x,t)= ∑_j ∫_0^t { c_j(s)/2^(2d) ∏_k [erf((l_{j,k} + R_j) / √(4D(t−s)))
# −erf((l_{j,k} −R_j)/√(4D(t−s)))]
# exp(−ke(t−s)) } ds
##
# Here, our sources are the amoebas which we take to be stationary circles of
# radius R_j. There are j \in {1, ..., N} sources
# and k \in {1, ..., d} dimensions (d=2 here).
# c_j(s) is the amount of cAMP that the jth source created at a time t-s.
# l_{j,k} is the distance between the jth source and point x, on the kth
# cartesian coordinate.
# We calculate γ(x,t) using numerical integration and numerical evaluation
# of the error function (erf). Both are fast to solve.
# We don't need the concentration everywhere, just where there are cells
# affected by it. Therefore, this is more efficient than solving the
# diffusion PDE for the entire space (Finite Elements Methods can do that).
# This scales with the number of sources (lesions) and since there is
# degradation, we needn't keep the entire history of the source emissions.

ke = 5.4  # degradation coefficient [time^-1] min^−1
kt = 0.9  # kt / h related to amount of cAMP exported from cell [mm^-1]
h = 0.025  # dimensionless, see above
D = 0.024  # diffusion coefficient [length^2 / time] mm^2 / min

Rmeans = sp.array([sp.zeros(d_env)])  # [length] mm or array with each centre
Rradii = sp.array([epsilon_Lsize / 2] * n_agents
                  )  # [length] mm or array with each radius

# chemo- sources
# cfns = [lambda x, t: 38 * 1 * 1000]


# Calculate the concentration at a point x in space.

# Helper function integrand_fun. The integrand in (1) Eq (8).
def integrand_fun(s, c_fn_j, d, loc_j, R_j, D, ke, t):
    G_int_space = c_fn_j(s) / (2 ** (2 * d))
    if t - s == 0:
        G_int_space *= 0
    else:
        G_int_space *= sp.prod(erf((loc_j + R_j) / (sp.sqrt(4 * D * (t - s)))) -
                               erf((loc_j - R_j) / (sp.sqrt(4 * D * (t - s)))))
    G_int_space *= sp.exp(-ke * (t - s))
    return G_int_space


# Input:
# X: array of points for which to estimate the fn (n x d_env)
# t: the time elapsed since chem_conc = 0 everywhere except source
# (i.e. time of system).
# Source emissions: c_fns
# (these should be getters in the Ddisc class for the cAMP production history)
# List of (N) functions with time as input, one for each source.
# Fn returns source emission for the source at time s.
#
# Output:
# chems: array of concentrations for each x point (n x 1)
def camp_conc(X, t, c_fns, Rmeans):
    X = sp.atleast_2d(X)
    if not X.shape[1] == d_env:
        print('Mismatch between X dimension and environment dimension.')
        return
    if not len(c_fns) == len(Rradii):
        print('Mismatch in number of sources between emission fns and radii.')
        return

    chems = sp.zeros(len(X))
    n_j = len(Rradii)

    for i, x in enumerate(X):
        # build distances l_{j, k}
        locs = sp.absolute(Rmeans - x)
        chem_sum = 0
        for j in range(n_j):
            arg_j = (c_fns[j],
                     d_env,
                     locs[j],
                     Rradii[j],
                     D,
                     ke,
                     t
                     )
            chem_sum += sp.integrate.quad(integrand_fun,
                                          max(0, t - t_hist), t,
                                          args=arg_j)[0]
        chems[i] = chem_sum
    return chems


# Function for concentration gradient at points x
#
# Helper function integrand_fun. The integrand in (1) Eq (8).
def grad_integrand_fun(s, c_fn_j, d, loc_j, R_j, D, ke, t, k):
    loc_j_cav = sp.absolute(loc_j[sp.arange(len(loc_j)) != k])
    # loc_j_k = sp.absolute(loc_j[k])
    G_int_space = c_fn_j(s) / (2 ** (2 * d))
    if t - s == 0:
        G_int_space *= 0
    # else:
    #     G_int_space *= (sp.exp(-(loc_j_k + R_j) ** 2 / (4 * D * (t - s))) -
    #                     sp.exp(-(loc_j_k - R_j) ** 2 / (4 * D * (t - s)))
    #                     )
    #     G_int_space *= sp.prod(erf((loc_j_cav + R_j) / sp.sqrt(4 * D * (t - s))) -
    #                            erf((loc_j_cav - R_j) / sp.sqrt(4 * D * (t - s))))
    #     G_int_space *= 1. / sp.sqrt(sp.pi * D * (t - s))
    #     G_int_space *= (int(loc_j[k] < 0) * 2 - 1)
    #     G_int_space *= sp.exp(-ke * (t - s))
    else:
        G_int_space *= (sp.exp(-(loc_j[k] + R_j) ** 2 / (4 * D * (t - s))) -
                        sp.exp(-(loc_j[k] - R_j) ** 2 / (4 * D * (t - s)))
                        )
        G_int_space *= sp.prod(erf((loc_j_cav + R_j) / sp.sqrt(4 * D * (t - s))) -
                               erf((loc_j_cav - R_j) / sp.sqrt(4 * D * (t - s))))
        G_int_space *= 1. / sp.sqrt(sp.pi * D * (t - s))
        G_int_space *= sp.exp(-ke * (t - s))
    return G_int_space


# Input:
# X: array of points for which to estimate the fn (n x d_env)
# t: the time elapsed since chem_conc = 0 everywhere except source
# (i.e. time of system).
# Source emissions: c_fns
# List of (N) functions with time as input, one for each source.
# Fn returns source emission for the source at time s.
#
# Output:
# chems: array of concentration gradient for each x point (n x d_env)
def grad_chem_conc(X, t, c_fns, Rmeans):
    X = sp.atleast_2d(X)
    if not X.shape[1] == d_env:
        print('Mismatch between X dimension and environment dimension.')
        return
    if not len(c_fns) == len(Rradii):
        print('Mismatch in number of sources between emission fns and radii.')
        return

    grad_chems = sp.zeros((len(X), d_env))
    n_j = len(Rradii)

    for i, x in enumerate(X):
        # build distances l_{j, k}
        locs = x - Rmeans
        for d in range(d_env):
            chem_sum = 0
            for j in range(n_j):
                arg_j = (c_fns[j],
                         d_env,
                         locs[j],
                         Rradii[j],
                         D,
                         ke,
                         t,
                         d
                         )
                chem_sum += sp.integrate.quad(grad_integrand_fun,
                                              max(0, t - t_hist), t,
                                              args=arg_j)[0]
            grad_chems[i, d] = chem_sum
    return grad_chems

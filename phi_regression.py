import scipy as sp
# from scipy import linalg as la
# from scipy.stats import norm

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# using GPflow (TensorFlow GPs)
import gpflow
from gpflow import transforms
# from gpflowopt import optim, BayesianOptimizer
# from gpflowopt.domain import ContinuousParameter
# from gpflowopt.design import LatinHyperCube
# from gpflowopt.acquisition import ExpectedImprovement
# from gpflowopt.objective import batch_apply
import pickle
import config

'''
def GP_prob(extra_points, X_points, y, y_meta=None, y_pred_meta=None):
    # ARTIFICIAL POINTS
    """
    x = sp.linspace(min(X_points[:, 0]), max(X_points[:, 0]), 10)
    # points in the x axis
    y_p = (sp.exp(x / 35 * 3) - 0.9) / (3 * 35)
    y_m = (sp.exp((x - 20) / 35 * 3) - 0.9) / (3 * 35)
    z_p = sp.ones((x.shape[0], 1))
    z_m = sp.zeros((x.shape[0], 1))
    print(y.shape, z_p.shape)
    X_points = sp.vstack((X_points,
                          sp.vstack((x, y_p)).T,
                          sp.vstack((x, y_m)).T))
    y = sp.vstack((y, z_p, z_m))
    """
    # Changing labels to {-1, 1} as required by pyGPs
    y[y == 0] = -1

    # Subsampling for hyperparameter optimization
    sample_size = min(10000, len(X_points))
    sample_idx = sp.random.choice(len(X_points),
                                  sample_size, replace=False)
    X_points_opt = X_points[sample_idx]
    y_opt = y[sample_idx]

    gp = pyGPs.GPC_FITC()
    gp.setData(x=X_points, y=y, value_per_axis=10)

    # m = pyGPs.mean.Zero()
    # m = pyGPs.mean.Linear(alpha_list=[-1, 10])
    # k = pyGPs.cov.RBFard(log_ell_list=[3.5, -2.5], log_sigma=3)
    k = pyGPs.cov.RBFard(log_ell_list=[3.5, -2.5], log_sigma=5)
    # m = pyGPs.mean.Zero()
    m = gp.meanfunc
    gp.setPrior(mean=m, kernel=k)

    # gp.optimize(numIterations=5)
    u = gp.u
    m = gp.meanfunc
    k_hyp = gp.covfunc.hyp
    print(gp.covfunc)
    print("Hyperparameter optimisation done.", k_hyp)

    """
    k = pyGPs.cov.RBFard(log_ell_list=k.hyp[0:1], log_sigma=k.hyp[-1])
    k.hyp = k_hyp

    gp = pyGPs.GPC_FITC()
    gp.setData(X_points, y)
    gp.setPrior(mean=m, kernel=k, inducing_points=u)
    """

    y_pred_mean, y_pred_sigma = gp.predict(extra_points)[:2]
    y_pred_sigma = (y_pred_sigma, y_pred_sigma)

    return y_pred_mean, y_pred_sigma, gp
# '''


# -------------------------
# GPflow
# """
def create_model(theta, X_points, y):
    var = theta[0]
    lengthsc = theta[1:]
    k = gpflow.kernels.RBF(input_dim=sp.shape(X_points)[1], ARD=True,
                           variance=var, lengthscales=lengthsc)

    # m = gpflow.models.VGP(X_points, y,
    #                       kern=k,
    #                       likelihood=gpflow.likelihoods.Bernoulli(),
    #                       )
    n_inducing = 9  # no of inducing points per input dimension
    Z_points = [sp.linspace(sp.amin(X_points[:, d]),
                            sp.amax(X_points[:, d]),
                            n_inducing) for d in range(sp.shape(X_points)[1])]
    Z_points = sp.vstack(Z_points).T

    m = gpflow.models.SVGP(X_points, y,
                           kern=k,
                           likelihood=gpflow.likelihoods.Bernoulli(),
                           Z=Z_points
                           )
    # m.likelihood.variance = 1e-1
    # print(m.read_trainables())

    return m


def GP_prob_flow(extra_points, X_points, y):
    """
    # OPTIMISATION
    @batch_apply
    def gpflow_opt(theta=None):
        theta = sp.atleast_2d(theta)

        for th in theta:
            try:
                model_tmp = create_model(th)
                neg_ll = - model_tmp.compute_log_likelihood()
                # print(neg_ll)
                return neg_ll
            except Exception:
                print("Exception.")
                pass
        raise RuntimeError("Failed indefinately.")

    def Bayesian_optimize(fx, n_iter=20, domain=None):
        if domain is None:
            domain = ContinuousParameter('variance', -5, 5)
        lhd = LatinHyperCube((2 * domain.size) ** 2, domain)
        X = lhd.generate()
        X = sp.vstack((X, sp.ones(domain.size)))
        Y = fx(X)
        print(X.shape, Y.shape, domain.size)
        # Y = sp.atleast_2d(Y)

        k_opt = gpflow.kernels.Matern52(domain.size, ARD=False)
        # k_opt = gpflow.kernels.RBF(input_dim=domain.size, ARD=False)

        m = gpflow.gpr.GPR(X, Y, kern=k_opt)
        # m.optimize()

        # Now create the Bayesian Optimizer
        ei = ExpectedImprovement(m)
        opt = optim.StagedOptimizer([optim.MCOptimizer(domain, 100),
                                     optim.SciPyOptimizer(domain)])
        optimizer = BayesianOptimizer(domain, ei, optimizer=opt)

        # Run the Bayesian optimization
        print("Latin Hypercube done... optimising.")
        with optimizer.silent():
            r = optimizer.optimize(fx, n_iter=n_iter)
        return r
    # """

    # Non-optimised model
    model = create_model(theta=sp.ones(sp.shape(X_points)[1] + 1),
                         X_points=X_points, y=y)
    print("NLL", - model.compute_log_likelihood())

    # simple optimisation for hyperparams
    # set model.kern.trainable = False to turn off hyperparameter opt.
    # note that optimisation is still necessary for the VGP approx to work
    model.kern.trainable = False
    gpflow.train.ScipyOptimizer().minimize(model, maxiter=200)
    print("NLL", - model.compute_log_likelihood())
    # model.optimize(n_restarts=20)

    """
    # Bayesian optimisation of hyperparameters (var and lengthscale)
    dom = ContinuousParameter('lvariance', 1e-3, 1e4) + \
        sp.sum([ContinuousParameter('llength{0}'.format(i), 1e-2, 1e1)
                for i in range(proj_dim)])
    print(dom)
    print()
    #
    Bayes_opt = Bayesian_optimize(gpflow_opt, n_iter=25, domain=dom)
    theta_opt = Bayes_opt['x'][0]

    model = create_model(theta=theta_opt)

    print("Optimum theta:")
    print(theta_opt)
    print("Optimum NLL", - model.compute_log_likelihood())
    # """

    y_pred_mean, y_pred_var = model.predict_y(extra_points)

    return y_pred_mean, y_pred_var, model


def plot_GP(xx, yy, y_plot, y_plot_var, X, Y):
    # Plotting scatter of observations, wireframe of GP mean
    fig = plt.figure()
    ax = fig.gca(projection='3d')               # 3d axes instance
    surf = ax.plot_wireframe(xx, yy, y_plot,      # data values (2D Arryas)
                             # rstride=2,           # row step size
                             # cstride=2,           # column step size
                             # cmap=cm.RdPu,        # colour map
                             # linewidth=1,         # wireframe line width
                             color='blue',
                             antialiased=True)

    """
    surf_2sigma_plus = ax.plot_wireframe(xx, yy, y_plot + 2 * sp.sqrt(y_plot_var),
                                         antialiased=True, color='green')

    surf_2sigma_minus = ax.plot_wireframe(xx, yy, y_plot - 2 * sp.sqrt(y_plot_var),
                                          antialiased=True, color='green')
    # """

    ax.scatter(X[:, 0], X[:, 1], Y,
               marker='x',
               c='r')
    ax.set_xlabel('Angle (rad)')
    ax.set_ylabel('dummy')
    ax.set_zlabel('Probability satisfaction for $\phi_{split}$')


def train_GP(run=True, plot=False):
    if run == 'both':
        m_L_prphi = sp.load('x_prphi_True.npy')
        m_L_prphi = sp.vstack((m_L_prphi, sp.load('x_prphi_False.npy')))
    else:
        m_L_prphi = sp.load('x_prphi_reltheta_{}.npy'.format(run))

    # Building sample
    sample_size = len(m_L_prphi)  # min(10000, len(m_L_prphi))
    sample_idx = sp.random.choice(len(m_L_prphi),
                                  sample_size, replace=False)
    m_L_prphi = m_L_prphi[sample_idx]

    X = m_L_prphi[:, :1]
    X = sp.cos(X)
    # X = X - 2 * sp.pi * sp.int_((X + sp.pi) // (2 * sp.pi))
    X = sp.hstack((X, sp.zeros((len(X), 1))))
    Y = m_L_prphi[:, -1]
    if sp.any(Y < 0):
        print("Y<0")
        Y[Y < 0] = 0
    if sp.any(Y > 1):
        print("Y>1")
        Y[Y > 1] = 1

    x = sp.linspace(min(X[:, 0]), max(X[:, 0]), 100)  # points in the x axis
    # points in the y axis
    y = sp.linspace(min(X[:, 1]) - 1e-3, max(X[:, 1]) + 1e-3, 100)
    xx, yy = sp.meshgrid(x, y)                # create the "base grid"
    x_pred = sp.column_stack((xx.ravel(), yy.ravel()))

    # GP for estimating phi_{RUN}
    y_gp = Y.reshape((len(Y), 1))

    y_pred, y_pred_var, gp = GP_prob_flow(x_pred,
                                          X,
                                          y_gp)
    print(y_pred_var[:5])
    y_pred = y_pred[:, 0]
    y_plot = y_pred.reshape(sp.shape(xx))
    y_plot_var = y_pred_var[:, 0].reshape(sp.shape(xx))

    # Saving trained GP
    # gp_dict = {}
    # gp_dict['model'] = gp.read_trainables()
    gp_dict = gp.read_values()
    pickle.dump(gp_dict, open('VGP_phi_{}.pkl'.format(str(run)), 'wb'))
    if plot:
        # plot_GP(xx, yy, y_plot, y_plot_var, X, Y)
        y_plot, _ = gp.predict_y(sp.column_stack((x, sp.zeros(len(x)))))
        plt.figure()
        if run:
            plt.title("Split ($y=1$)", fontsize=14)
        else:
            plt.title("De novo ($y=0$)", fontsize=14)

        plt.plot(X[:, 0], Y, 'kx', mew=2)
        plt.plot(x, y_plot[:, 0])
        if hasattr(gp, 'feature') and hasattr(gp.feature, 'Z'):
            Z = gp.feature.Z.read_value()
            plt.plot(Z[:, 0], 0.5 * sp.ones(len(Z)), 'ro', ms=2)
        plt.ylabel("Satisfaction probability ($\Psi_y$) for $\phi$.", fontsize=12)
        plt.xlabel("$\cos(\\theta_{rel} (s_1^{t-1}, \\nabla\\gamma_j^{t-1}))$", fontsize=12)
        plt.tight_layout()
        # moving average line
        """
        X_sort_idx = sp.argsort(X[:, 0])
        X_sorted = X[X_sort_idx, 0]
        Y_sorted = Y[X_sort_idx]
        Y_smooth = sp.zeros_like(Y_sorted)
        for i in range(49, len(X_sorted) - 50):
            Y_smooth[i] = sp.mean(Y_sorted[i - 49: i + 51])
        plt.plot(X_sorted, Y_smooth, c='r')
        """


def load_SVGP(fnames, d=2):
    gp_dicts = []
    for fname in fnames:
        with open(fname, 'rb') as f:
            gp_dicts += [pickle.load(f)]

    gps = []
    X = sp.rand(100, d)
    y = sp.rand(100, 1)
    print(X.shape, y.shape)
    th = sp.ones(d + 1)

    for gp_d in gp_dicts:
        # k = gpflow.kernels.RBF(input_dim=d, ARD=True)
        # k.assign(gp_d['kern'])

        m = create_model(th, X, y)
        m.assign(gp_d)
        gps += [m]
    return gps


if __name__ == '__main__':
    # """
    train_GP(run=True, plot=True)
    train_GP(run=False, plot=True)
    # """
    # train_GP(run='both', plot=True)

    # testing load_SVGP function
    """
    gps = load_SVGP(('VGP_phi_False.pkl', 'VGP_phi_True.pkl'))
    x = sp.linspace(-5, 6, 200)
    x_pred = sp.column_stack((x, sp.zeros(len(x))))
    for gp in gps:
        y_pred_mean, _ = gp.predict_y(x_pred)
        y_plot = y_pred_mean[:, 0]

        plt.figure()
        # plt.title("Run: True")
        plt.ylim(0, 1)
        plt.plot(x, y_plot)
        if hasattr(gp, 'feature') and hasattr(gp.feature, 'Z'):
            Z = gp.feature.Z.read_value()
            plt.plot(Z[:, 0], 0.5 * sp.ones(len(Z)), 'ro', mew=0, ms=2)
    # """

    plt.show()

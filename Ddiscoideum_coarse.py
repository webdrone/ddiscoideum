import scipy as sp
import os
from scipy import stats
import config

from phi_regression import load_SVGP


class Tumbling_angle_dist(stats.rv_continuous):

    def _pdf(self, x):
        return (0.5 * (1 + sp.cos(x)) * sp.sin(x))


class Ddiscoideum:
    # USER SETTINGS
    # (MODEL TO BE USED, PARAMETERS)

    # Specifying SSA model (Pseudopodium creation)
    """
    smodel = "Q_pseudopodium.npy"  # numpy array file with Q matrix
    smodel_dir = os.path.dirname(os.path.realpath(__file__))
    smodel = os.path.join(smodel_dir, smodel)
    # """

    # Loading gps
    gp_false, gp_true = load_SVGP(('VGP_phi_False.pkl', 'VGP_phi_True.pkl'))

    # Weiner variable for ... nothing really yet
    def dW(dt):
        return sp.random.normal(loc=0.0, scale=sp.sqrt(dt))

    def __init__(s, state=config.state, parameters=None,
                 L=None, grad_L=None,
                 pos=sp.zeros(config.d_env), speed=config.speed,
                 outfile=config.outname):
        # Starting stochpy SSA object (Flagellum/motor)
        # s.Q_model = sp.load(Ddiscoideum.smodel)
        # s.Q_update = config.Q_update

        # Loading gps
        # s.gp_false, s.gp_true = load_SVGP(('VGP_phi_False.pkl', 'VGP_phi_True.pkl'))
        # s.gp_false, s.gp_true = Ddiscoideum.gp_false, Ddiscoideum.gp_true

        s.outfile = outfile

        if state is None:
            s.state = (0, 1)
            # in {0, 1, 2, 3, 4, 5} x {0, 1, 2, 3, 4, 5}
            # n(t), n(t-1)
            s.state_coarse = True  # split
        else:
            s.state = state
            s.state_coarse = False  # de novo

        # Setting time, location, and velocity
        s.t = 0.0
        # keeping track of running, tumbling times
        s.t_run = []
        s.t_tumble = [0.0]
        s.run_flag = 0
        s.pos = sp.array(pos)
        s.speed = speed
        s.tumble()
        s.trajectory = sp.asmatrix(sp.hstack((0.0, s.pos)))
        s.camp_history = [(-1e-6, 0.0), (1e-6, 0.0)]
        # for MG equations
        s.beta = 0
        s.rho = sp.rand()

    def simulate(s, state=None, L=None, grad_L=None,
                 n_agents=1, dt=config.dt):
        # Calculating new Q_model according to L, grad_L
        # s.Q_current = s.Q_update(s.Q_model, L, grad_L)

        # Simulating
        N_traj = n_agents

        if s.run_flag:
            gp = Ddiscoideum.gp_true
            # gp = s.gp_true
        else:
            gp = Ddiscoideum.gp_false
            # gp = s.gp_false

        # Drawing new state according to gp prediction of Bernoulli trial param.
        th_grad = sp.arctan2(grad_L[1], grad_L[0])
        th = th_grad - s.get_theta()
        # th = th - 2 * sp.pi * sp.int_((th + sp.pi) // (2 * sp.pi))
        th = sp.cos(th)
        state_prob, _ = gp.predict_y(sp.column_stack((th, 0)))

        # """
        # \phi ::= (th_ag - th_grad < pi / 2)
        arr_th_rel = th_grad - s.get_theta(sp.arange(6))
        arr_th_rel = arr_th_rel - 2 * sp.pi * sp.int_((arr_th_rel + sp.pi) // (2 * sp.pi))
        # """
        if sp.rand() < state_prob:
            state_coarse_new = True
            # """
            # \phi ::= (th_ag - th_grad < pi / 2)
            arr = sp.arange(6)[sp.where(sp.absolute(arr_th_rel) < sp.pi / 2)]
            arr = arr[sp.logical_not(arr == s.state[0])]
            state_new = sp.random.choice(a=arr)
            # """

            """
            # \phi = run_flag(agent)
            state_new = sp.random.choice(a=(1, -1))
            state_new = (s.state[0] + state_new) % 6
            # """
        else:
            state_coarse_new = False
            # """
            # \phi ::= (th_ag - th_grad < pi / 2)
            # we include the probability 1/21 for denovo to be directly opposite
            arr = sp.arange(6)[sp.absolute(arr_th_rel) >= sp.pi / 2]
            arr = arr[sp.logical_not(arr == s.state[0])]
            state_new = sp.random.choice(a=arr)

            # """

            """
            # \phi = run_flag(agent)
            state_new = sp.random.choice(a=(2, -2))
            state_new = (s.state[0] + state_new) % 6
            # """


        # Changing model state to reflect last state simulated
        s.state_coarse = state_coarse_new
        s.state = (state_new, s.state[0])
        # Updating camp production
        s.beta, s.rho = s.camp_production_rate(L, dt)

        s.move(dt=dt)

        # recording camp production rate in agent history
        while (s.camp_history[-1][0] - s.camp_history[1][0]) > config.t_hist:
            del s.camp_history[0]
        s.camp_history += [(s.t, s.beta)]

        # Writing DTMC to file
        if s.outfile is not None:
            species_save = sp.hstack((s.state, L, grad_L))
            sp.savetxt(s.outfile, species_save)

    def camp_production_rate(s, gamma, dt):
        # beta = 1e2

        # cAMP production rate according to MG equations
        # solving ODE with forward Euler
        rho = s.rho
        beta = s.beta
        dt_euler = 0.01

        # Helper constants for rho forward Euler
        f1 = (config.k1 + config.k2 * gamma) / (1 + gamma)
        f2 = (config.k_1 + config.lambda1 * config.k_2 * gamma) / (
            1 + config.lambda1 * gamma)

        for i in range(int(dt // dt_euler)):
            # forward Euler for beta ODE
            Y = rho * gamma / (1 + gamma)
            phi = (config.lambda2 + Y ** 2) / (config.lambda3 + Y ** 2) * 1800

            dbeta = dt_euler * (phi - (config.ki + config.kt) * beta)
            beta = beta + dbeta

            # forward Euler for rho ODE
            drho = dt_euler * (f2 * (1 - rho) - f1 * rho)
            rho = rho + drho

        return beta, rho

    def move(s, dt=0.05):

        s.t += dt

        th_new = s.get_theta()
        th_old = s.get_theta(s.state[1])
        th = th_new - th_old
        th = th - 2 * sp.pi * sp.int_((th + sp.pi) // (2 * sp.pi))
        # if new pseudopod is > pi/2 then de novo
        if sp.absolute(th) > sp.pi / 2:
        # if not s.run_flag:
            s.tumble(dt=dt)
            # print(s.t)
            # Collecting tumbling times
            if s.run_flag:
                s.t_tumble += [dt]
                s.run_flag = 0
            else:
                s.t_tumble[-1] += dt
        # otherwise, split
        else:
            s.run(dt=dt)
            # Collecting run times
            if s.run_flag:
                s.t_run[-1] += dt
            else:
                s.t_run += [dt]
                s.run_flag = 1

        # add new position to history
        traj = sp.hstack((s.t, s.pos))
        s.trajectory = sp.vstack((s.trajectory, traj))

    # In the original model, there is no actual run/tumble dependence.
    # State of the DTMC fully determines the motion.
    # but we can change this to be a run / turn function depending on the
    # split/denovo property.
    def run(s, dt=0.05):
        s.vel = config.R_mat(s.state[0] * sp.pi / 3) @ sp.array([s.speed, 0])
        s.pos += s.vel * dt

    def tumble(s, dt=0.05):
        # Assuming theta_dist gives absolute values
        s.vel = config.R_mat(s.state[0] * sp.pi / 3) @ sp.array([s.speed, 0])
        s.pos += s.vel * dt

    def get_camp_rate(s, t=0):
        # go through history and return camp rate at time t
        for c in range(len(s.camp_history) - 1):
            if s.camp_history[c][0] <= t < s.camp_history[c + 1][0]:
                # linear interpolation
                m = (s.camp_history[c + 1][1] - s.camp_history[c][1]) / (
                    s.camp_history[c + 1][0] - s.camp_history[c][0])
                x = t - s.camp_history[c][0]
                beta = m * x + s.camp_history[c][1]
                camp = beta * config.kt / (2 * config.h)
                return camp
                # return 1

        print("Error -- camp history not accessible.")
        print(t, s.camp_history)
        return None

    def get_theta(s, state=None):
        if state is None:
            state = s.state[0]
        # normalising th \in(-pi, pi)
        th = sp.pi * (state / 3 - 2 * ((state / 3 + 1) // 2))
        return th
